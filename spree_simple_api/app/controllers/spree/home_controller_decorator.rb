module Spree
  HomeController.class_eval do
  	 skip_before_action :verify_authenticity_token 
    

    def initialize_shopify
      return "https://00c6c2948948b3e3be5563095817787a:52663c3875819dbb63f48f71d91fb6f7@giveit.myshopify.com/admin/"
    end
  
    def api_shopify_pagenate
      puts params[:category]
           @result = HTTParty.get(initialize_shopify+"products/count.json?query&product_type="+params[:category], 
                :headers => { 'Content-Type' => 'application/json' } )
      render json: { data: (@result['count']/10).ceil}
      
    end

    def api_add_to_shopify_cart
      if cart = Spree::Cart.create(line_item_params)
        render json: {status: :success, data: cart }
      else
        render json: {status: :failed, error: "unable to create"}
      end
    end

    def api_my_lineitem
      if cart = Spree::Cart.where("user_id = ? AND status = ? ",params[:user_id], "pending").as_json(only: [:variant_id, :quantity])
        render json: {status: :success, cart: cart}
      else
        render json: {status: :failed, error: "cart not found"}
      end
    end


    def api_shopify_product_by_category
      
       @result = HTTParty.get(initialize_shopify+"products.json?query&product_type="+params[:category]+"&limit=10&page="+params[:page], 
                :headers => { 'Content-Type' => 'application/json' } )
      render json: {status: :success, products: @result['products']}
    end

    def api_vmoney_authentication
      if Spree::UserAuthentication.where("user_id = ? AND provider = ?",params[:user_id],"vmoney").exists?
        render json: {status: :success, data: "true"}
      else
        render json: {status: :success, data: "false"}
      end
    end

    def api_remove_vmoney_authentication  
      if vmoney_auth = Spree::UserAuthentication.find_by_id(params[:auth_id])
        vmoney_auth.destroy
        render json: {status: :success, data: "successfully deeted"}
      else
        render json: {status: :failed, error: "authentication not found"}
      end
    end

    def api_add_vmoney_userauthentication
      if userauth = Spree::UserAuthentication.create(vmoney_params)
        render json: {status: :success, data: userauth}
      else
        render json: {status: :success, data: "unable to create"}
      end
    end

    def api_search_name
      if auth = Spree::UserAuthentication.where("name ILIKE ?", "%#{params[:name]}%").as_json(include: 
        {user: {include:[
          {campaigns:{include: [
            {comments:{include:{user:{include: :user_authentications}}}},
            {pledges:{include:{user:{include: :user_authentications}}}},
            {contributions:{include:{user:{include: :user_authentications}}}}
            ]}},
            :user_authentications]}})
        
        render json: {status: :success, data: auth}
      else
        render json: {status: :failed}
      end
    end

    def api_check_countdown
      if campaign = Spree::Campaign.find_by_id(params[:campaign_id])
        if campaign.countdown <= 5
          render json: {status: :success, data: :true, countdown: campaign.countdown}
        else
          render json: {status: :success, data: :false, countdown: campaign.countdown}
        end
      else
        render json: {status: :failed, data: "campaign not found"}
      end
    end


    def api_my_notifications
        user = Spree::User.find_by_id(params[:user_id]).as_json(only: [:user_id], 
          include: [{comment_notifs:{include: [{campaign:{include: [
            {comments:{include:{user:{include: :user_authentications}}}},
            {pledges:{include:{user:{include: :user_authentications}}}},
            {contributions:{include:{user:{include: :user_authentications}}}},
            {user:{include: :user_authentications}}
            ]}} ,{comment: { only: [:body], include: {user:{include: :user_authentications}}}}]}},
                    {contribution_notifs:{include: [{campaign:{include: [
            {comments:{include:{user:{include: :user_authentications}}}},
            {pledges:{include:{user:{include: :user_authentications}}}},
            {contributions:{include:{user:{include: :user_authentications}}}},
            {user:{include: :user_authentications}}
            ]}} ,{contribution: { only: [:amount], include: {user:{include: :user_authentications}}}}]}},
                    {pledge_notifs:{include: [{campaign:{include: [
            {comments:{include:{user:{include: :user_authentications}}}},
            {pledges:{include:{user:{include: :user_authentications}}}},
            {contributions:{include:{user:{include: :user_authentications}}}},
            {user:{include: :user_authentications}}
            ]}} ,{pledge: { only: [:amount], include: {user:{include: :user_authentications}}}}]}}])
        render json: {status: :success, data: user}
    end

    def api_add_contribution
      if contribution = Spree::Contribution.create(contribution_params)
        campaign = Spree::Campaign.find_by_id(params[:contribution][:campaign_id])
        notif = Spree::ContributionNotif.create(user_id: campaign.user_id ,campaign_id: campaign.id ,contribution_id: contribution.id)
        render json: {status: :success, data: contribution ,notif: notif}
      else
        render json: {status: :failed, msg: "failed to add contribution."}
      end
    end

    def api_user_profile
      if Spree::User.find_by_id(params[:user_id]).update_attributes(user_params)
         user = Spree::User.find_by_id(params[:user_id])
         user_auth =  user.user_authentications.first
         user_auth.name = params[:name]
          if user_auth.save 
            render json: {status: :created, user: user.as_json(include: :user_authentications)}
          else
            render json: {status: :failed, data: "unable to update data."}
          end
      else
          render json: {status: :failed, data: "unable to update data."}
      end
    end

    def api_my_actions
      if user = Spree::User.find_by_id(params[:user_id])
        pledge = user.pledges.as_json(include: {campaign: {include: [{user: {include: :user_authentications}}]}})
        contribution = user.contributions.as_json(include: {campaign: {include: [{user: {include: :user_authentications}}]}})
        render json: {status: :created, pledges: pledge ,contributions: contribution}
      else
        render json: {status: :failed, data: "user not found"}
      end

    end

    def api_user_info
      if user = Spree::User.find_by_id(params[:user_id])
        render json: {status: :success , user_info: user}
      else
        render json: {status: :failed, error: "user not found"}
      end
    end

    def api_by_category
      if taxon = Spree::Taxon.where('lower(name) = ?', params[:category_name].downcase).first 
         products = taxon.products.as_json(include: [:taxons,:variants,:master,:properties, :images],methods: [:price,:available_on])
         render json: {status: :created, products: products}
      else
        render json: {status: :failed, data: "taxon not found."}
      end
    end

    def api_shared_campaign
      if Spree::SharedCampaign.where("user_id = ? and campaign_id = ?",params[:user_id], params[:campaign_id]) == []
        @shared = Spree::SharedCampaign.new
        @shared.user_id = params[:user_id]
        @shared.campaign_id = params[:campaign_id]
        if @shared.save
          render json: {status: :created}
        else
          render json: {status: :failed, data: "unable to save data."}
        end
      else
        render json: {status: :failed, data: "you can only share once."}
      end
    end

    def api_update_campaign
      if @campaign = Spree::Campaign.find_by_id(params[:campaign_id])
        @campaign.update_attributes(campaign_params)
        if @campaign.save
          render json: {status: :created}
        else
          render json: {status: :failed, data: "unable to save data."}
        end
      else
        render json: {status: :failed, data: "campaign not found"}
      end
    end

    def api_update_campaign_countdown
      if @campaign = Spree::Campaign.find_by_id(params[:campaign_id])
        @campaign.countdown = params[:countdown]
        if @campaign.save
          render json: {status: :created}
        else
          render json: {status: :failed, data: "unable to save data."}
        end
      else
        render json: {status: :failed, data: "campaign not found"}
      end
    end

    def api_update_pledge_countdown
      if @pledge = Spree::Pledge.find_by_id(params[:pledge_id])
        @pledge.countdown = params[:countdown]
        if @pledge.save
          render json: {status: :created}
        else
          render json: {status: :failed ,data: "unable to save data."}
        end
      else
        render json: {status: :failed, data: "pledge not found"}
      end
    end

    def api_remove_comment
      if comment = Spree::Comment.find_by_id(params[:comment_id])
        comment.destroy
        render json: {status: :created}
      else
        render json: {status: :failed}
      end
    end

    def api_successful_message
      if  campaign = Spree::Campaign.find_by_id(params[:campaign_id])
          comments     = campaign.comments
          pledges      = campaign.pledges
          contributions = campaign.contributions
          @emails       = []
          # comments.each do |comment|
          #   @emails = @emails.push(comment.user.email)
          # end

          pledges.each do |pledge|
            @emails = @emails.push(pledge.user.email)
          end

          contributions.each do |contribution|
            @emails = @emails.push(contribution.user.email)
          end

          @emails.uniq.each do |email|
            @result = HTTParty.post("http://104.131.98.56/ypi/api/send_notif", 
                   :body => { :user => {   :name   => "" ,
                                           :emails => email,
                                           :header => "Hooray! your friend #{campaign.user.user_authentications.first.name} have finished his/her Campaign successfully",
                                           :body   => params[:message]
                                            } }.to_json,
                  :headers => { 'Content-Type' => 'application/json' } )       
          end

          render json: {status: :created, data: "Message has been sent successfully."}
        else
          render json: {status: :failed, data: "campaign not found."}
        end
    end

    def api_country_state
      countries = Spree::Country.all.as_json(include: :states) 
      render json: {status: :created, countries: countries}
    end

    def api_visited_campaigns
      if @campaigns = Spree::VisitedCampaign.where("user_id = ?",params[:user_id])
        @campaigns = @campaigns.as_json(include: {campaign: {include: [{comments: {include: {user:{include: :user_authentications}}}},
          {pledges: {include: {user:{include: :user_authentications}}}}, 
          {contributions: {include: {user:{include: :user_authentications}}}}]}})
        render json: {status: :created, campaigns: @campaigns}
      else
        render json: {status: :failed, data: "no visited campaign yet."}
      end  
    end

    def api_my_cart
        if user = Spree::User.find_by_id(params[:id])
          cart = user.carts
          render json: {status: :success, cart: cart}
        else
          render json: {status: :failed, error: "user not found"}
        end   
    end

    def api_remove_from_cart
        if cart = Spree::Order.find_by_id(params[:order_id])
          cart.destroy
          render json: {status: :deleted}
        else
          render json: {status: :failed ,data: "cart not found."}
        end
    end

    def api_update_time_visited
        @visited_ids = params[:visited_campaigns]
        @visited_ids.each do |visited_id|
          if visited_campaign = Spree::VisitedCampaign.find_by_id(visited_id)
             visited_campaign.time_visited = DateTime.now
          end
        end

        render json: {status: :created}
    end

    def api_add_friend
      @user_id = params[:friendship][:user_id]
      @friend_id = params[:friendship][:friend_id]

      if (@friend = Spree::Friendship.where("user_id = ? AND friend_id = ? OR user_id = ? AND friend_id = ?",@user_id,@friend_id,@friend_id,@user_id)) != []
       @friend.first.status = "true"
        if @friend.first.save
          render json: {status: :created}
        else
          render json: {status: :failed, data: "unable to save data."}
        end
      else
        if @friends = Spree::Friendship.create(friend_params)   
          @friends.status = "true"
          if @friends.save
            render json: {status: :created, friend: @friends}
          else
            render json: {status: :failed, data: "unable to save data."}
          end
        else
          render json: {status: :failed, data: "unable to create friendship." }
        end
      end  

    end

    def api_add_friend_from_facebook
      if facebook_friends_ids = params[:facebook_friends]
         facebook_friends_ids.each do |facebook_friend_id|
          if user_auth = Spree::UserAuthentication.find_by_uid(facebook_friend_id)
            if Spree::Friendship.where("user_id = ? AND friend_id = ? OR user_id = ? AND friend_id = ?",params[:user_id],user_auth.user.id,user_auth.user.id,params[:user_id]) == []
              @friends = Spree::Friendship.create(user_id: params[:user_id], friend_id: user_auth.user.id ,status: "true")
            end
          end
         end
         api_my_friends

      else
        render json: {status: :failed, data: "incorrect parameters."}
      end
    end

    def api_my_friends
      if @campaign = Spree::Campaign.where("countdown > ? AND user_id = ? ",0,params[:user_id]).as_json(include: 
        [{user:{include: :user_authentications}},
        {comments:{include: {user:{include: :user_authentications}}}},
        {pledges:{include: {user:{include: :user_authentications}}}},
        {contributions:{include: {user:{include: :user_authentications}}}},
        {shared_campaigns:{include: {user:{include: :user_authentications}}}}])

        if data = Spree::User.find_by_id(params[:user_id]).as_json(include:
         [{inverse_friendships:{include:
          {user:{include: [:user_authentications,{campaigns:{include: 
          [{user:{include: :user_authentications}},
          {comments:{include: {user:{include: :user_authentications}}}},
          {pledges:{include: {user:{include: :user_authentications}}}},
          {shared_campaigns:{include: {user:{include: :user_authentications}}}},
          {contributions:{include: {user:{include: :user_authentications}}}}]}}]}}}},
          {friendships:{include:
          {friend:{include: [:user_authentications, {campaigns:{include: 
          [{user:{include: :user_authentications}},
          {comments:{include: {user:{include: :user_authentications}}}},
          {pledges:{include: {user:{include: :user_authentications}}}},
          {shared_campaigns:{include: {user:{include: :user_authentications}}}},
          {contributions:{include: {user:{include: :user_authentications}}}}]}}]}}}}])

          #response = Spree::Variant.where('spree_variants.sale_price is not null').uniq
          render json: {status: :created, my_friends: data, my_campaign: @campaign }
        else
          render json: {status: :failed, data: "user not found."}
        end
      else
        render json: {status: :failed, data: "campaign not found."}
      end

      
    end

    def api_remove_friend
      @user_id = params[:user_id]
      @friend_id = params[:friend_id]
      if (@friends = Spree::Friendship.where("user_id = ? AND friend_id = ? OR user_id = ? AND friend_id = ?",@user_id,@friend_id,@friend_id,@user_id)) != []
        @friends.first.status = "false"
        if @friends.first.save
          render json: {status: :created}
        else
          render json: {status: :failed, data: "unable to save."}
        end
      else
        render json: {status: :failed, data: "unknown friend."}
      end
        
    end

    def api_all_profiles
        profiles = Spree::User.all.as_json(include: :user_authentications)
        render json:{status: :created, profiles: profiles}
    end

  	def api_products
       @data = Spree::Variant.all.as_json(include: { product:{include: [:taxons,
       {master: {include: [:option_values, :images]}}]}})
  		render json:{status: "success" ,variants: @data }
  	end

    def api_add_wish

     if Spree::Wishlist.find_by_user_id(params[:user_id]).equal?(nil)
         @wishlist = Spree::Wishlist.new
         @wishlist.user_id = params[:user_id]
         @wishlist.name = "my wishlist"
         @wishlist.save
     else
         @wishlist = Spree::Wishlist.find_by_user_id(params[:user_id])
     end
        
        Spree::WishedProduct.create(variant_id: params[:variant_id],
        wishlist_id: @wishlist.id, title: params[:title], price: params[:price])
        @wishlist = @wishlist.as_json(only: [:name], include: 
                              { wished_products: { only: [:id], include: 
                                {variant: {include: 
                                  {product:{include:  
                                    {images:{include: :viewable}}}}}}}})
        
        render json:{status: :created, wishlist: @wishlist}

    end

    def api_create_comment
       if comment = Spree::Comment.create(comment_params)
        campaign = Spree::Campaign.find_by_id(params[:comment][:campaign_id])
        notif = Spree::CommentNotif.create(user_id: campaign.user_id ,campaign_id: campaign.id ,comment_id: comment.id)
          if (visited = Spree::VisitedCampaign.where("user_id = ? AND campaign_id = ?",params[:comment][:user_id],params[:comment][:campaign_id])) != []
              visited.first.time_visited = DateTime.now
              visited.first.save
          else
             Spree::VisitedCampaign.create(user_id: params[:comment][:user_id], campaign_id: params[:comment][:campaign_id], time_visited: DateTime.now)
          end
          @campaign = Spree::Campaign.find(params[:comment][:campaign_id]).as_json(include:
                                         {comments:{include: {user:{include: :user_authentications}}}}) 
          campaign = Spree::Campaign.find_by_id(params[:comment][:campaign_id])
          campaign.updated_at = DateTime.now
          if campaign.save
              render json: {status: :created, campaign: @campaign , notif: notif}
          else
            render json: {status: :failed, data: "unable to save data."}
          end
        else
          render json: {status: :failed, data: "cannot create comment"}
        end
    end

    def api_add_pledge
        @pledge = Spree::Pledge.create(pledge_params)
        campaign = Spree::Campaign.find_by_id(params[:pledge][:campaign_id])
        notif = Spree::PledgeNotif.create(user_id: campaign.user_id ,campaign_id: campaign.id ,pledge_id: @pledge.id)
        @pledge.countdown = 3
       if @pledge.save
          if (visited = Spree::VisitedCampaign.where("user_id = ? AND campaign_id = ?",params[:pledge][:user_id],params[:pledge][:campaign_id])) != []
              visited.first.time_visited = DateTime.now
              visited.first.save
          else
             Spree::VisitedCampaign.create(user_id: params[:comment][:user_id], campaign_id: params[:comment][:campaign_id], time_visited: DateTime.now)
          end
          @campaign = Spree::Campaign.find_by_id(params[:pledge][:campaign_id])
          @campaign.updated_at = DateTime.now  
          if @campaign.save
            @pledge = @pledge.as_json(include: {user:{include: :user_authentications}})
            render json: {status: :created, pledge: @pledge}
          else
            render json: {status: :failed, data: "unable to save campaign."}
          end
       else
          render json: {status: :failed, data: "unable to save pledge."}
       end
    end

    # def api_normal_login
    #   if user =  User.find_by(email: params[:email])
    #     if @user_hash == BCrypt::Engine.hash_secret(params[:password], @user.password_salt.to_s)
    #     render json: {status: :created}
    #   else
    #     render json: {status: :failed}
    #   end
    # end


    def api_remove_wishproduct
      if wish = Spree::WishedProduct.find_by_id(params[:wishproduct_id])
        wish.destroy
        render json: {status: :created}
      else
        render json: {status: :failed, data: "wished product not found."}
      end
    end

    def api_add_campaign
        if (campaign = Spree::Campaign.where("countdown > ? AND user_id = ? ",0,params[:campaign][:user_id])) != []
          render json:{status: :success ,msg: "you still have an active campaign.
           You can only process one campaign at a time."}
        else
          campaign = Spree::Campaign.create(campaign_params)
          campaign.countdown = 30
          campaign.funds = 0
          if campaign.save
            Spree::VisitedCampaign.create(user_id: params[:campaign][:user_id], campaign_id: campaign.id, time_visited: DateTime.now)
            @campaign = campaign.as_json(include: {user:{include: :user_authentications}}) 
            render json:{status: "success" ,campaign: @campaign}
          else
            render json:{status: :failed, data: "unable to save campaign."}
          end
        end
    end

    def api_my_campaign
      if @campaign = Spree::Campaign.where("countdown > ? AND user_id = ? ",0,params[:user_id]).as_json(include: 
        [{user:{include: :user_authentications}},
        {comments:{include: {user:{include: :user_authentications}}}},
        {pledges:{include: {user:{include: :user_authentications}}}},
        {contributions:{include: {user:{include: :user_authentications}}}},
        {shared_campaigns:{include: {user:{include: :user_authentications}}}}])

        render json: {status: :success, campaign: @campaign}
      else
        render json: {status: :failed, data: "campaign not found."}
      end

    end

    def api_all_campaigns
      @campaigns = Spree::Campaign.where("countdown > ?",0).as_json(include: [ {user:{include: :user_authentications}},
                                                          {comments:{include: {user:{include: :user_authentications}}}},
                                                          {pledges:{include: {user:{include: :user_authentications}}}},
                                                          {contributions:{include: {user:{include: :user_authentications}}}},
                                                          {shared_campaigns:{include: {user:{include: :user_authentications}}}}])
      render json: {status: :created, campaigns: @campaigns}
    end

    def api_login_facebook
     		
          if authentication = Spree::UserAuthentication.find_by_provider_and_uid("facebook",params[:uid])
             @data =  authentication.user.as_json(only: [:email ,:id] , include: 
                                                { wishlists:{ only: [:name], include: 
                                                  { wished_products: { only: [:id], include: 
                                                    {variant: {include: 
                                                      {product:{include: 
                                                        {images:{include: :viewable}}}}}}}}}})
             render json: {status: :success, user: authentication.user ,data: @data}
          else
            user = Spree::User.new
            user.email = params[:email]
            user.password = "123456"
             if user.save
                auth = Spree::UserAuthentication.new
                auth.user_id = user.id
                auth.name = params[:name]
                auth.provider = "facebook"
                auth.uid = params[:uid]
                @data = []
                if auth.save
                  @result = HTTParty.post("http://104.131.98.56/ypi/api/send_notif", 
                 :body => { :user => {   :name    => auth.name, 
                                         :email   => user.email,
                                         :header  => "You have successfully Logged in",
                                         :body    => "Please visit http://45.55.90.88/, and have your dream gadget."
                                          } }.to_json,
                :headers => { 'Content-Type' => 'application/json' } )

                   render json: {status: :created, user: user ,data: @data}
                else
                  render json: {status: :failed ,data: "unable to save authentication."}
                end
            else
                render json: {status: :failed, data: "unable to save user."}
            end
          end
    end

    def api_my_wishlist
      if user = Spree::User.find_by_id(params[:user_id])
              wishlist = user.wishlists.as_json(include: 
                  { wished_products: { only: [:id], include: 
                    {variant: {include: 
                      {product:{include: 
                        {images:{include: :viewable}}}}}}}})
        render json: {status: :created , wishlist: wishlist}
      else
        render json: {status: :failed, data: "user not found."}
      end
    end

    def api_change_password
      if user = Spree::User.find_by_id(params[:user_id])
        user.generate_spree_api_key!
        user.password = params[:password]
        user.save

         if wishlists = user.wishlists
                  wishlists.each do |wish|
                    @products = wish.wished_products
                  end
         end
         @result = HTTParty.post("http://104.131.98.56/ypi/api/send_notif", 
                 :body => { :user => {   :name    => user.user_authentications.first.name, 
                                         :email   => user.email,
                                         :header  => "You have successfully Updated your password",
                                         :body    => "Please visit http://45.55.90.88/, and have your dream gadget."
                                          } }.to_json,
                :headers => { 'Content-Type' => 'application/json' } )

         render json: {status: :created, user: user ,wished_products: @products}
      else
        render json: {status: :failed, data: "user not found"}
      end
    end

    private

    def line_item_params
      params.require(:line_item).permit(:user_id, :variant_id, :quantity, :image_link, :title, :price)
    end
    
    def user_params
      params.require(:spree_user).permit(:email, :location, :gender, :birthday)
    end

    def vmoney_params
      params.require(:vmoney).permit(:user_id, :provider, :email)
    end

    def campaign_params
      params.require(:campaign).permit(:user_id, :product_id, :title ,:video_link, :product_price, :variant_id, :image_link)
    end

    def comment_params
      params.require(:comment).permit(:user_id, :campaign_id, :body)
    end

    def pledge_params
      params.require(:pledge).permit(:user_id, :campaign_id, :amount)
    end

    def contribution_params
      params.require(:contribution).permit(:user_id, :campaign_id, :amount)
    end
    
    def friend_params
      params.require(:friendship).permit(:user_id, :friend_id, :status)
    end

     def current_order_params
        { currency: current_currency, guest_token: cookies.signed[:guest_token], store_id: current_store.id, user_id: try_spree_current_user.try(:id) }
     end
  end
end
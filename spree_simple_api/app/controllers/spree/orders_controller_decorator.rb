Spree::Admin::OrdersController.class_eval do
  def resend
     @result = HTTParty.post("http://104.131.98.56/ypi/api/send_notif", 
                 :body => { :user => {   :name => "", 
                                         :email => try_spree_current_user.email,
                                         :header => "Successfully Resend Order",
                                         :body => "You have successfully resend your order."
                                          } }.to_json,
                :headers => { 'Content-Type' => 'application/json' } )

     flash[:success] = Spree.t(:order_email_resent)

        redirect_to :back
  end
end

class Spree::ApplicationMailer < ActionMailer::Base
  default from: "eebasadre20@gmail.com"
  #layout 'mailer'

  def youpound_it(user)
    @user = user
    mail(to: @user.email, subject: 'Sample Email')
  end
end

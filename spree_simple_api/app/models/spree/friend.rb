module Spree
  class Spree::Friend < ActiveRecord::Base
  #Add your active record associations
  #validations and model methods
  belongs_to :user, class_name: "Spree::User"
  belongs_to :verification ,class_name: "Spree::Verification"
  
  end
end
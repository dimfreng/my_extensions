module Spree
  class Spree::SharedCampaign < ActiveRecord::Base
  
  belongs_to :user, class_name: "Spree::User"
  belongs_to :campaign ,class_name: "Spree::Campaign"	

  end
end
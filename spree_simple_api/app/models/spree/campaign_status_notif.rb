module Spree
  class Spree::CampaignStatusNotif < ActiveRecord::Base
  
  belongs_to :campaign,class_name: "Spree::Campaign"	
  belongs_to :user, class_name: "Spree::User"
  end
end
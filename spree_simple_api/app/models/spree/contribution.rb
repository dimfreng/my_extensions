module Spree
  class Spree::Contribution < ActiveRecord::Base
  #Add your active record associations
  #validations and model methods
  belongs_to :campaign ,class_name: "Spree::Campaign"
  belongs_to :user, class_name: "Spree::User"
  belongs_to :contribution_notif, class_name: "Spree::ContributionNotif"
  end
end
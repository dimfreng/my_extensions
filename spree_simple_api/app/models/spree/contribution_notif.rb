module Spree
  class Spree::ContributionNotif < ActiveRecord::Base
  
  belongs_to :contribution, class_name: "Spree::Contribution"
  belongs_to :campaign,class_name: "Spree::Campaign"	
  belongs_to :user, class_name: "Spree::User"
  end
end
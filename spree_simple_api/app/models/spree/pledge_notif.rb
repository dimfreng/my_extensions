module Spree
  class Spree::PledgeNotif < ActiveRecord::Base
  
  belongs_to :pledge, class_name: "Spree::Pledge"
  belongs_to :campaign,class_name: "Spree::Campaign"	
  belongs_to :user, class_name: "Spree::User"
  end
end
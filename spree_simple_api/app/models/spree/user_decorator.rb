module Spree
  Spree::User.class_eval do
	  
	  has_many :shared_campaigns, class_name: "Spree::SharedCampaign"
	  has_many :campaigns ,class_name: "Spree::Campaign", :dependent => :destroy
	  has_many :pledges ,class_name: "Spree::Pledge", :dependent => :destroy
	  has_many :contributions ,class_name: "Spree::Contribution", :dependent => :destroy
	  has_many :comments ,class_name: "Spree::Comment", :dependent => :destroy
	  has_many :friendships ,class_name: "Spree::Friendship" , :dependent => :destroy
	  has_many :friends, :through => :friendships 
	  has_many :inverse_friendships, :class_name => "Spree::Friendship", :foreign_key => "friend_id" 
	  has_many :inverse_friends, :through => :inverse_friendships, :source => :user
	  has_many :visited_campaigns, class_name: "Spree::VisitedCampaign" ,:dependent => :destroy
	  has_many :comment_notifs, class_name: "Spree::CommentNotif", :dependent => :destroy
	  has_many :contribution_notifs, class_name: "Spree::ContributionNotif", :dependent => :destroy
	  has_many :pledge_notifs, class_name: "Spree::PledgeNotif", :dependent => :destroy
	  has_many :campaign_status_notifs, class_name: "Spree::CampaignStatusNotif", :dependent => :destroy
	  has_many :carts, class_name: "Spree::Cart", :dependent => :destroy

  end
end


module Spree
  class Spree::CommentNotif < ActiveRecord::Base
  
  belongs_to :comment, class_name: "Spree::Comment"
  belongs_to :campaign,class_name: "Spree::Campaign"	
  belongs_to :user, class_name: "Spree::User"
  end
end
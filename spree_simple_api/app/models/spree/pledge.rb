module Spree
  class Spree::Pledge < ActiveRecord::Base
  #Add your active record associations
  #validations and model methods
  belongs_to :campaign ,class_name: "Spree::Campaign"
  belongs_to :user, class_name: "Spree::User"
  belongs_to :pledge_notif, class_name: "Spree::PledgeNotif"
  end
end
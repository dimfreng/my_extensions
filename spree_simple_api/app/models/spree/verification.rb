module Spree
  class Spree::Verification < ActiveRecord::Base
  #Add your active record associations
  #validations and model methods
  belongs_to :user, class_name: "Spree::User"
  has_many :friends, class_name: "Spree::Friend"
  end
end
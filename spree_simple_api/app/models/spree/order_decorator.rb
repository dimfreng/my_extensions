Spree::Order.class_eval do
     def send_cancel_email
      
      @order = Spree::Order.find(id)
      @result = HTTParty.post("http://104.131.98.56/ypi/api/send_notif", 
                 :body => { :user => {   :name => "", 
                                         :email => @order.email,
                                         :header => "You have cancel your order",
                                         :body => "You can choose other product to Checkout."
                                          } }.to_json,
                :headers => { 'Content-Type' => 'application/json' } )
    end


    def deliver_order_confirmation_email
     
      @order = Spree::Order.find(id)
      
      @result = HTTParty.post("http://104.131.98.56/ypi/api/send_notif", 
                 :body => { :user => {   :name => "", 
                                         :email => @order.email,
                                         :header => "Successfully Checkout your Order #{@order.number}",
                                         :body => "This is the total payment of your transaction,Php#{@order.payment_total}"
                                          } }.to_json,
                :headers => { 'Content-Type' => 'application/json' } )
       update_column(:confirmation_delivered, true)

    end
end

module Spree
  class Spree::Comment < ActiveRecord::Base
  #Add your active record associations
  #validations and model methods
  belongs_to :user, class_name: "Spree::User"
  belongs_to :campaign ,class_name: "Spree::Campaign"
  belongs_to :comment_notif, class_name: "Spree::CommentNotif"
  end
end
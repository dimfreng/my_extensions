module Spree
  class Spree::Friendship < ActiveRecord::Base
  	belongs_to :user ,class_name: "Spree::User"
  	belongs_to :friend, class_name: "Spree::User"
  end
end
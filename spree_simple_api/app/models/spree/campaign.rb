module Spree
  class Spree::Campaign < ActiveRecord::Base
  
  has_many :shared_campaigns, class_name: "Spree::SharedCampaign"
  belongs_to :user, class_name: "Spree::User"	
  belongs_to :visited_campaign, class_name: "Spree::VisitedCampaign"
  has_many :pledges ,class_name: "Spree::Pledge", :dependent => :destroy
  has_many :contributions ,class_name: "Spree::Contribution", :dependent => :destroy
  has_many :comments, class_name: "Spree::Comment", :dependent => :destroy
  has_many :comment_notifs, class_name: "Spree::CommentNotif", :dependent => :destroy
  has_many :contribution_notifs, class_name: "Spree::ContributionNotif", :dependent => :destroy
  has_many :pledge_notifs, class_name: "Spree::PledgeNotif", :dependent => :destroy
  has_many :campaign_status_notifs, class_name: "Spree::CampaignStatusNotif", :dependent => :destroy
  end
end
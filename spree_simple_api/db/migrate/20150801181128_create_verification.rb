class CreateVerification < ActiveRecord::Migration
  def up
    create_table :spree_verifications do |t|
    t.integer :user_id
    t.index :user_id
    end
  end

  def down
  	drop_table :spree_verifications
  end

end

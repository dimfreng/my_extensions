class CreateCampaign < ActiveRecord::Migration
   def up
    create_table :pledges do |t|
    	t.integer :user_id
    	t.integer :campaign_id
    	t.integer :amount
    	t.integer :countdown
    	t.index :user_id
    	t.index :campaign_id
    	t.timestamps
    end
  end

  def down
  	drop_table :pledges
  end
end

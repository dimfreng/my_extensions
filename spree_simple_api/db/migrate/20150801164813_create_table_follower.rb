class CreateTableFollower < ActiveRecord::Migration
  def up
    create_table :spree_friends do |t|
    t.integer :user_id
    t.integer :verification_id
    t.string  :status
    t.index   :user_id	
    t.index   :verification_id
    end
  end

  def down
  	drop_table :spree_friends
  end
end

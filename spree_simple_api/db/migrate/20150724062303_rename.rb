class Rename < ActiveRecord::Migration
   def up
    rename_table :campaigns, :spree_campaigns
   end

	 def down
	    rename_table :spree_campaigns, :campaigns
	 end
end

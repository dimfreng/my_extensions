class AddColumnProductPrice < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns, :product_price, :string
  end

  def down
  	remove_column :spree_campaigns, :product_price
  end
end

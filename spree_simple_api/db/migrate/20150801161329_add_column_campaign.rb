class AddColumnCampaign < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns, :facebook_id, :integer
  	add_column :spree_campaigns, :facebook_name, :string
  end

  def down
  	remove_column :spree_campaigns ,:facebook_name 
  	remove_column :spree_campaigns ,:facebook_id
  end
end

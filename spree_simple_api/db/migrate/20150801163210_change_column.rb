class ChangeColumn < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns, :video_link, :text	
  	change_column :spree_campaigns, :facebook_id, :string
  end

  def down
  	remove_column :spree_campaigns, :video_link
  	change_column :spree_campaigns, :facebook_id, :integer
  end
end

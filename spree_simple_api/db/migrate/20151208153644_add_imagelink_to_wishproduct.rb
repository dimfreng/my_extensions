class AddImagelinkToWishproduct < ActiveRecord::Migration
  def up
  	add_column :spree_wished_products, :image_link ,:text
  end

  def down 	 	
  	remove_column :spree_wished_products, :image_link
  end
end

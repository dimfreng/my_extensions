class RenameBubby < ActiveRecord::Migration
  def up
  	rename_table :buddy_mappers ,:spree_buddy_mappers
  end

  def down
  	rename_table :spree_buddy_mappers, :buddy_mappers
  end
end

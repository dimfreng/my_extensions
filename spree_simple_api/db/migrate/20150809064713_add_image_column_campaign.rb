class AddImageColumnCampaign < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns ,:image_link ,:text
  end

  def down
  	remove_column :spree_campaigns ,:image_link 
  end
end

class ChangeCampaignElement < ActiveRecord::Migration
  def up
  	remove_column :spree_campaigns  ,:description
  	remove_column :spree_campaigns  ,:image_link
  	remove_column :spree_campaigns ,:product_path
  	add_column    :spree_campaigns ,:variant_id ,:integer
  end

  def down
  	add_column :spree_campaigns  ,:description ,:text
  	add_column :spree_campaigns  ,:image_link ,:text
  	add_column :spree_campaigns ,:product_path ,:text
  	remove_column    :spree_campaigns ,:variant_id
  end
end

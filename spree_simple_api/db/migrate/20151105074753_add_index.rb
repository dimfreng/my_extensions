class AddIndex < ActiveRecord::Migration
  def up
  	add_index :spree_pledge_notifs, :user_id 
  	add_index :spree_contribution_notifs, :user_id 
  	add_index :spree_comment_notifs, :user_id 
  	add_index :spree_campaign_status_notifs, :user_id 
  end

  def down
  	remove_index :spree_campaign_status_notifs, :user_id
  	remove_index :spree_comment_notifs, :user_id
  	remove_index :spree_contribution_notifs, :user_id
  	remove_index :spree_pledge_notifs, :user_id
  end
end

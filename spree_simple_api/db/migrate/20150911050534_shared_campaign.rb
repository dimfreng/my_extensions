class SharedCampaign < ActiveRecord::Migration
  def up
  	 create_table :spree_shared_campaigns do |t|
    	t.integer  :user_id
    	t.integer  :campaign_id
    	t.index :user_id
    	t.index :campaign_id
    	t.timestamps
    end
  end

  def down
  	drop_table :spree_shared_campaigns
  end
end

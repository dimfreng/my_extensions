class ChangeDataTypeOfProductId < ActiveRecord::Migration
  def up
  	change_column :spree_campaigns, :product_id, :integer, :limit => 20
  	change_column :spree_campaigns, :variant_id, :integer, :limit => 20
  	change_column :spree_campaigns, :product_price, :integer, :limit => 20
  end

  def down
  	change_column :spree_campaigns, :product_id, :integer
  	change_column :spree_campaigns, :variant_id, :string
  	change_column :spree_campaigns, :product_price, :integer
  end
end

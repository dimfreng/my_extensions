class CreateTableCampaign < ActiveRecord::Migration
  def up
    create_table :campaigns do |t|
    	t.integer :user_id
    	t.integer :product_id
    	t.string  :title
    	t.text    :description
    	t.integer :countdown
    	t.integer :funds
    	t.timestamps
    end
  end

  def down
  	drop_table :campaigns
  end
end

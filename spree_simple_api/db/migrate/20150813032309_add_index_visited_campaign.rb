class AddIndexVisitedCampaign < ActiveRecord::Migration
  def up
  	add_index :spree_visited_campaigns, :user_id
  	add_index :spree_visited_campaigns, :campaign_id
  end

  def down
  	remove :spree_visited_campaigns, :user_id
  	remove :spree_visited_campaigns, :campaign_id
  end
end

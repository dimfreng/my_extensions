class UpdateFriendshipModel < ActiveRecord::Migration
  def up
  	change_column :spree_friendships, :user_id, :integer, :null => false
  	change_column :spree_friendships, :friend_id, :integer, :null => false
  end

  def down
  	change_column :spree_friendships, :user_id, :integer, :null => true
  	change_column :spree_friendships, :friend_id, :integer, :null => true
  end
end

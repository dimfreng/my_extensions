class AddUserId < ActiveRecord::Migration
  def up
  	add_column :spree_pledge_notifs, :user_id, :integer 
  	add_column :spree_contribution_notifs, :user_id, :integer 
  	add_column :spree_comment_notifs, :user_id, :integer 
  	add_column :spree_campaign_status_notifs, :user_id, :integer 
  end

  def down
  	remove_column :spree_campaign_status_notifs, :user_id
  	remove_column :spree_comment_notifs, :user_id
  	remove_column :spree_contribution_notifs, :user_id
  	remove_column :spree_pledge_notifs, :user_id
  end
end

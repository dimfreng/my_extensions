class ChangeAgainTheDataType < ActiveRecord::Migration
  def up
  	remove_index :spree_campaigns, :product_id
  end

  def down
  	add_index :spree_campaigns, :product_id
  end
end

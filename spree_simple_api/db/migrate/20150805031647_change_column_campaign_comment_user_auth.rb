class ChangeColumnCampaignCommentUserAuth < ActiveRecord::Migration
  def up
  	remove_column :spree_comments  ,:facebook_id
  	remove_column :spree_comments  ,:facebook_name
  	remove_column :spree_campaigns ,:facebook_name
  	remove_column :spree_campaigns ,:facebook_id
  	add_column    :spree_user_authentications ,:name ,:string
  end

  def down
  	add_column :spree_comments  ,:facebook_id ,:string
  	add_column :spree_comments  ,:facebook_name ,:string
  	add_column :spree_campaigns ,:facebook_name ,:string
  	add_column :spree_campaigns ,:facebook_id ,:string
  	remove_column    :spree_user_authentications ,:name
  end
end

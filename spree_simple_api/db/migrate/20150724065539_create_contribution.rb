class CreateContribution < ActiveRecord::Migration
   def up
    create_table :contributions do |t|
    	t.integer :user_id
    	t.integer :campaign_id
    	t.integer :amount
    	t.index :user_id
    	t.index :campaign_id
    	t.timestamps
    end
  end

  def down
  	drop_table :contributions
  end
end

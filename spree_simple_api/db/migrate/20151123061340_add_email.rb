class AddEmail < ActiveRecord::Migration
   def up
  	add_column :spree_user_authentications, :email, :string 
  end

  def down
  	remove_column :spree_user_authentications, :email
  end
end

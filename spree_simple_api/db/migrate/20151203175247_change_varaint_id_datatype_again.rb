class ChangeVaraintIdDatatypeAgain < ActiveRecord::Migration
   def up
  	change_column :spree_campaigns, :variant_id, :string
  end

  def down
  	change_column :spree_campaigns, :facebook_id, :float
  end
end
